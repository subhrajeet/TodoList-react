import React, { Component } from "react";
import { renderToStaticMarkup } from "react-dom/server";
import TodoItems from "./TodoItems";
import "./TodoList.css";
class TodoList extends Component {
  constructor(props, context){
    super(props, context);
    this.state={
      items:[]
    }
    this.addItem=this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.editItem = this.editItem.bind(this);
  }
  addItem(e){
    e.preventDefault();
    if(this.state.editKey){
      this.saveEditedText();
      return;
    }
    var itemArray = this.state.items;
    if (this.inputElement.value !== '') {
      itemArray.unshift({
        text:this.inputElement.value,
        key:Date.now()
      })
      this.setState({
        items:itemArray
      })
      this.divRef.insertAdjacentHTML("beforeend", this.inputElement.value+renderToStaticMarkup(<p className="textcolor">has added successfully</p>));
      this.inputElement.value='';
      setTimeout( () => {
          this.divRef.innerHTML='';
      }, 3000);
    }
  }
  deleteItem(key) {
    const result = window.confirm('Are you sure to delete this item');
    if (result) {
      var filteredItems = this.state.items.filter(function (item) {
        return (item.key !== key);
      });
     
      this.setState({
        items: filteredItems
      });
    }
  }
  editItem(key){
    this.state.items.map(item =>{
      if (item.key==key) {
        this.inputElement.value=item.text;
      }
    })
    this.setState({editKey: key});
  }
  saveEditedText(){
    let value = this.inputElement.value;
    this.setState(prevState => ({
      items: prevState.items.map(el => {
        if(el.key == prevState.editKey)
          return Object.assign({}, el, {text: value});
         return el;
      }),
      editKey: ''
    }));
    this.divRef.insertAdjacentHTML("beforeend", this.inputElement.value+renderToStaticMarkup(<p className="textcolor"> has updated successfully</p>));
    this.inputElement.value='';
    setTimeout( () => {
         this.divRef.innerHTML='';
    }, 3000);
  }
  render() {
    return (
      <div className="todoListMain">
        <div className="header" id="parentDiv">
          <div className="pageHeading" dangerouslySetInnerHTML={{ __html: "Todo Demo Application" }}></div>
          <div className="wrapper">
            <div ref={divEl => {
            this.divRef = divEl;
          }}></div>
            <form onSubmit={this.addItem}>
              <input ref={(a)=>this.inputElement=a} placeholder="enter task">
              </input>
              <button type="submit">{this.state.editKey? "Update": "Add"}</button>
            </form>
            <TodoItems entries={this.state.items} delete={this.deleteItem} edit={this.editItem}/>
          </div>
        </div>
      </div>
    );
  }
}
 
export default TodoList;